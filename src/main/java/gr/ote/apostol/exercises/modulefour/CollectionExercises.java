package gr.ote.apostol.exercises.modulefour;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class CollectionExercises {

    private static final int MAX_NUMBERS = 50;

    // Used in Exercises 2, 3, 4
    private static void print(String label, List<String> list) {
        System.out.print(label + ": [");
        for (String s : list) {
            System.out.print(s + ", ");
        }
        System.out.println("]");
    }

    // Used in Exercise 5
    private static void print(String label, Iterator<String> iter) {
        System.out.print(label + ": [");

        while (iter.hasNext()) {
            System.out.print(iter.next() + ", ");
        }
        System.out.println("]");
    }

    // Following three methods are used in exercise 8 & 9
    private static Integer[] readNumbers(String prompt) {
        Integer[] numbers = new Integer[MAX_NUMBERS];
        int i = 0;

        Scanner userInput = new Scanner(System.in);

        System.out.println(prompt);
        while (i < MAX_NUMBERS && userInput.hasNext()) {
            String read = userInput.next();
            if (!read.equals("n")) {
                numbers[i++] = Integer.parseInt(read);
                System.out.println(prompt);
            }
            else break;
        }
        userInput.close();

        // Use arraycopy to only collect user-inputs and NOT default null items
        // that will cause NullPointerException in sort()!
        Integer[] copyTo = new Integer[i];
        System.arraycopy(numbers, 0, copyTo, 0, i);

        return copyTo;
    }

    private static Integer[] sort(Integer[] arr) {
        // A null comparator signifies "natural ordering" eg. Integer's compareTo()
        Arrays.sort(arr, null);
        return arr;
    }

    // Generic signature so it can be used for both Integers and Strings!
    // Also used in Exercise 1
    private static <T> void print(String label, T[] arr) {
        System.out.print(label + ": [");
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + (i+1 < arr.length ? ", " : "]"));
        }
        System.out.println();
    }

    public static void main(String[] args) {

        System.out.println("***** Exercise 1: Create an array of strings and add 10 elements");
        String[] tenStrings = new String[10];
        for (int i = 0; i < tenStrings.length; i++) {
            tenStrings[i] = String.valueOf("tenStrings".charAt(i));
        }
        print("String[] tenStrings", tenStrings);
        System.out.println();

        System.out.println("***** Exercise 2: Create a linked list and add 10 elements");
        List<String> ll = new LinkedList<>();
        Collections.addAll(ll, tenStrings);
        print("LinkedList<String> ll", ll);
        System.out.println();

        System.out.println("***** Exercise 3: Create an array list and add 10 elements");
        List<String> al = new ArrayList<>(Arrays.asList(tenStrings));
        print("ArrayList<String> al", al);
        System.out.println();

        System.out.println("***** Exercise 4: using ArrayList for deletions by index");
        al.remove(3);
        al.remove(7-1); // Take into account compaction after initial deletion
        print("al after removing 3rd, 7th items", al);
        System.out.println();

        System.out.println("***** Exercise 5: iteration with forEach() and Iterator ");
        print("ll via Iterator", ll.iterator());
        System.out.print("ll via forEach(): [");
        ll.forEach(e -> System.out.print(e + ", "));
        System.out.println("]");
        System.out.println();

        System.out.println("***** Exercise 6: efficient data-structures for random access?");
        System.out.println("ArrayList, Array, Vector");
        System.out.println();

        System.out.println("***** Exercise 7: " +
            "Which is the least appropriate list collection to remove elements?");
        System.out.println("ArrayList");
        System.out.println();

        System.out.println("***** Exercise 8: array sorter of user-input");
        System.out.println("Hint: provide up to " + MAX_NUMBERS + " integers, or type an 'n' to stop!");
        Integer[] userNumbers = readNumbers("Give a number: ");
        print("User-supplied numbers (sorted)" , sort(userNumbers));
        System.out.println();

        System.out.println("***** Exercise 9: minimum number in collected user-input");
        System.out.println("This will reuse previously input numbers, which are also sorted!");
        System.out.println("Min number supplied by user is: " + userNumbers[0]);
    }
}
