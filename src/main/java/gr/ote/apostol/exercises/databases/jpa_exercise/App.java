package gr.ote.apostol.exercises.databases.jpa_exercise;

import gr.ote.apostol.exercises.databases.jpa_exercise.model.Address;
import gr.ote.apostol.exercises.databases.jpa_exercise.model.Order;
import gr.ote.apostol.exercises.databases.jpa_exercise.model.Product;
import gr.ote.apostol.exercises.databases.jpa_exercise.utils.Util;

import java.util.Arrays;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class App {

    public static void main(String[] args) {
        // Create entityManagerFactory from the Exercise Persistence Unit
        EntityManagerFactory factory = Persistence.createEntityManagerFactory( "Exercise" );
        // Create entityManager
        EntityManager entityManager = factory.createEntityManager();

        Util.createTablesForExercise();

        // Join a transaction, otherwise nothing is saved in the database!
        entityManager.getTransaction().begin();

        Address address = new Address();
        address.setStreet("ethnikis antistasews");
        address.setNumber(22);

        // Save the address
        entityManager.persist(address);

        Product pen = new Product();
        pen.setName("pen");
        pen.setDescription("a beautiful pen");

        Product book = new Product();
        book.setName("book");
        book.setDescription("a beautiful book");

        /*
           Why only the book?
           Is this just an omission or on purpose to demonstrate difference
           of transient vs managed entities.
         */
        // Save the book
        entityManager.persist(book);
        // ...and pen if an omission!
        //entityManager.persist(pen);

        // Create a new order with the address and the products
        Order order = new Order();
        order.setAddress(address);
        order.setPrice(175.7);

        // Note how bidirectional relationships need to be maintained on both sides in Java!
        book.setOrder(order);
        pen.setOrder(order);
        order.getProducts().addAll(Arrays.asList(book, pen));

        entityManager.persist(order);

        // Print all the orders from the database
        entityManager.createQuery("select o from Order o", Order.class)
            .getResultList()
            .forEach(System.out::println);
        /*
          Will print (formatting is my own):
            Order(
                products=[
                    Product(id=1, name=book, description=a beautiful book),
                    Product(id=null, name=pen, description=a beautiful pen)
                ],
                id=1,
                orderDate=2022-09-21,
                price=175.7,
                address=Address(id=1, street=ethnikis antistasews, number=22)
            )

           Note that if pen wasn't persisted it would stay transient (no product row in DB)
           but still be printed here as Hibernate persistence context acts as a cache and
           returned order will be merged into same "order" object ref we created above!

         */

        // "Demarcates" end of transaction
        entityManager.getTransaction().commit();
        entityManager.close();

        System.out.println("****** Done *******");
    }
}
