package gr.ote.apostol.exercises.databases.jpa_exercise.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class Util {

    public static void createTablesForExercise() {
        try (Connection connection = DriverManager.getConnection("jdbc:mysql://localhost/sample", "user", "pwd");
             Statement stmt = connection.createStatement()) {

            String sql = "DROP TABLE IF EXISTS PRODUCTS_EX ";
            try {
                stmt.executeUpdate(sql);
            } catch (Exception e) {
                e.printStackTrace();
            }

            sql = "DROP TABLE IF EXISTS ORDERS_EX ";
            try {
                stmt.executeUpdate(sql);
            } catch (Exception e) {
                e.printStackTrace();
            }

            sql = "DROP TABLE IF EXISTS ADDRESSES_EX ";
            try {
                stmt.executeUpdate(sql);
            } catch (Exception e) {
                e.printStackTrace();
            }

            sql = "CREATE TABLE IF NOT EXISTS ADDRESSES_EX " +
                "(id INTEGER not NULL AUTO_INCREMENT, " +
                " street VARCHAR(255), " +
                " street_number INTEGER , " +
                " PRIMARY KEY ( id ))";

            stmt.executeUpdate(sql);

            sql = "CREATE TABLE IF NOT EXISTS ORDERS_EX " +
                "(id INTEGER not NULL AUTO_INCREMENT, " +
                " order_date date, " +
                " price  double , " +
                " address_id  INTEGER , " +
                " PRIMARY KEY ( id )," +
                " FOREIGN KEY (address_id) REFERENCES ADDRESSES_EX(ID))";

            stmt.executeUpdate(sql);


            sql = "CREATE TABLE IF NOT EXISTS PRODUCTS_EX " +
                "(id INTEGER not NULL AUTO_INCREMENT, " +
                " name VARCHAR(255), " +
                " description  VARCHAR(255) , " +
                " order_id  INTEGER , " +
                " PRIMARY KEY ( id )," +
                " FOREIGN KEY (order_id) REFERENCES ORDERS_EX(ID))";

            stmt.executeUpdate(sql);


        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}
