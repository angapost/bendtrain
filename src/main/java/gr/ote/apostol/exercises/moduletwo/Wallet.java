package gr.ote.apostol.exercises.moduletwo;

/**
 * Represents an electronic wallet of a single currency (say Euros).
 * Could be easily extended to hold a <code>Set&lt;Money&gt;</code> instances
 * for representing a wallet holding multiple currencies.
 */
public class Wallet {
    private Money money;

    public Wallet(Money money) {
        this.money = money;
    }

    protected Wallet deposit(int wholeUnits, int cents) {
        long amount = money.inCents(wholeUnits, cents);

        if (amount > 0) {
            money.setAsCents(money.getAsCents() + amount);
            return this; // Provide what is called a "fluent" interface
        }
        return withdraw(wholeUnits, cents);
    }

    protected Wallet withdraw(int wholeUnits, int cents) {
        long amount = money.inCents(wholeUnits, cents);

        long moneyInCents = money.getAsCents();
        if (amount > 0 && moneyInCents >= amount) {
            money.setAsCents(moneyInCents - amount);
            return this;
        }
        if (amount < 0) {
            return deposit(wholeUnits, cents);
        }
        return this;
    }

    protected Money getMoney() {
        return money;
    }
}
