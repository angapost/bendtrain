package gr.ote.apostol.exercises.moduletwo;

/**
 * A monetary amount in a certain currency unit. Represents money in its smallest
 * unit (cents) to perform exact calculations!
 * For convenience most method signatures take monetary amounts as pairs of
 * the form: <em>(wholeUnits, cents)</em> to emulate a <strong>decimal</strong>
 * type as is often used in DBs or business applications.
 *
 * <p>Note: use of floating point types such as float/double for representing money
 * is quite inappropriate as it loses accuracy and introduces rounding issues!
 */
public class Money {
    private final int centsInCurrencyUnit;
    private final char currency;
    private long amountInCents;

    public long inCents(int wholeUnits, int cents) {
        return (wholeUnits * centsInCurrencyUnit) + cents;
    }

    public Money() {
        this.currency = '\u20AC'; // Default currency is €
        this.centsInCurrencyUnit  = 100;
        this.amountInCents = 0;
    }

    public Money(long amountInCents) {
        this();
        this.amountInCents = amountInCents;
    }

    public Money(int wholeUnits, int cents) {
        this();
        this.amountInCents = inCents(wholeUnits, cents);
    }

    public Money(int wholeUnits, int cents, char currency, int centsInCurrencyUnit) {
        this.currency = currency;
        this.centsInCurrencyUnit = centsInCurrencyUnit;
        this.amountInCents = inCents(wholeUnits, cents);
    }

    public long getAsCents() {
        return this.amountInCents;
    }

    public long getWholeUnits() {
        return this.amountInCents / centsInCurrencyUnit;
    }

    public long getUnitCents() {
        return this.amountInCents % centsInCurrencyUnit;
    }

    public void setAsCents(long amountInCents) {
        this.amountInCents = amountInCents;
    }

    public void set(int wholeUnits, int cents) {
        this.amountInCents = inCents(wholeUnits, cents);
    }

    public String toString() {
        return "Money(" + this.getWholeUnits() + "," + this.getUnitCents() + currency + ")";
    }
}
