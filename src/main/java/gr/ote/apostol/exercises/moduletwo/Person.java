package gr.ote.apostol.exercises.moduletwo;

// Module 2 exercise: problem modelling with objects
public class Person {
    // Name in this simple setting is used as an identifier so it should not
    // be changeable after being associated with a person instance
    private final String name;

    private Wallet billFold = new Wallet( new Money(500, 0));

    public Person(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void purchase(int priceUnits, int priceCents) {
        billFold.withdraw(priceUnits, priceCents);
    }

    // Override equals() to properly treat name as identification for a person instance
    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof Person)) {
            return false;
        }
        final Person other = (Person) o;
        final Object thisName = this.getName();
        final Object otherName = other.getName();
        if (thisName == null ? otherName != null : !thisName.equals(otherName)) {
            return false;
        }
        return true;
    }

    // Whenever equals() is overridden hashCode() has also to follow suit!
    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final Object name = this.getName();
        result = result * PRIME + (name == null ? 43 : name.hashCode());
        return result;
    }

    public String toString() {
        return "Person(name=" + this.getName() + ", purchasingPower=" +
            this.billFold.getMoney().toString() + ")";
    }

    public static void main(String[] args) {
        Person anghelos = new Person("Anghelos");
        Person meetMyclone = new Person("Anghelos");

        System.out.println("*** Person equality test (name defines identity)!");
        System.out.println("anghelos : " + anghelos.toString());
        System.out.println("meetMyclone : " + meetMyclone.toString());
        System.out.println("anghelos == meetMyclone? => " + (anghelos == meetMyclone));
        System.out.println("anghelos.equals(meetMyclone)? => " + anghelos.equals(meetMyclone));
        System.out.println();

        System.out.println("*** Before his spending spree!");
        System.out.println(anghelos.toString());

        anghelos.purchase(132, 56);

        System.out.println("*** After his spending spree: anghelos.purchase(132, 56)!");
        System.out.println(anghelos.toString());
    }
}
