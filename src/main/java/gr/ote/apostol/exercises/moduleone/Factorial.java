package gr.ote.apostol.exercises.moduleone;

import java.text.NumberFormat;

// Exercise 1
public class Factorial {

    private boolean firstCall = true;
    private long initialParameter;

    public static void main(String[] args) {

        int n = RunnerUtils.readInt("Give a natural number up to 20");

        if (n < 1) {
            System.out.println("Warning: factorial(n) is defined for n >= 1");
            return;
        }
        if (n > 20) {
            System.out.println("Warning: overflows will occur for this number!\n");
        }

        System.out.println("*** Factorial with a 'for' loop ***");
        factorial(n);

        System.out.println("*** Factorial with a 'while' loop ***");
        factorial2(n);

        System.out.println("*** Factorial with a 'do-while' loop ***");
        factorial3(n);

        System.out.println("*** Recursive Factorial ***");
        new Factorial().recurse(n);
    }

    /**
     * Computes the <em>factorial</em> of an natural number <code>n</code> that is:
     * <pre>
     *     factorial(n): 1 * 2 * 3 * ... * n
     * </pre>
     * using a for loop.
     *
     * <p>Note that "wider" <code>long</code> type is used for the result to allow
     * for more robust experimentation as factorials can easily grow large really fast!
     * Even with a long result type we reach near its magnitude limits at 20!
     * That is factorial(21) overflows! From then on we get somewhat "random" large
     * values (both positive and negative) and at factorial(66) a zero occurs which
     * then stays forever!
     *
     * @param n a natural number (positive int) to compute its factorial
     */
    public static void factorial(int n) {
        /*
            Initialize with multiplication's identity element,
            which is conveniently also the factorial(1)!
         */
        long factorialOfN = 1;

        for (int i = 1; i <= n; i++) {
            factorialOfN *= i;

            // Gets more interesting to get a sense of how fast it grows by
            // printing interim results and also tracing loop's internals!
            ppr(i, factorialOfN);
        }

        printFinalResult(factorialOfN);
    }

    public static void factorial2(int n) {
        long factorialOfN = 1;
        int i = 1;

        while (i <= n) {
            factorialOfN *= i;
            ppr(i, factorialOfN);
            ++i;
        }

        printFinalResult(factorialOfN);
    }

    public static void factorial3(int n) {
        long factorialOfN = 1;
        int i = 1;

        do {
            factorialOfN *= i;
            ppr(i, factorialOfN);
            i += 1;
        } while (i <= n);

        printFinalResult(factorialOfN);
    }

    /**
     * <em>Lispers</em> or <em>Clojurians</em> out there rejoice! Who needs loops?
     * Use Computer Science's little darling named <em>Recursion</em>, that is
     * a function that calls itself on a reduced instance of the same problem.
     * This is a technique especially popular in <em>functional programming</em>
     * but except in special cases is not as performant or memory efficient as
     * iteration despite its clarity and mathematical beauty! Especially in Java
     * where there is no <em>Tail Call Optimization</em>.
     *
     * <p>Specifically a recursive formulation for <em>Factorial</em> is:
     * <pre>
     *     Factorial(n) = n * Factorial(n-1)
     * </pre>
     *
     * @param n a natural number (positive int) to compute its factorial
     * @return the factorial
     */
    public long recurse(int n) {
        // Use OO state management lessons to track initial value of n passed here!
        if (this.firstCall) {
            this.firstCall = false;
            this.initialParameter = n;
        }

        /*
          This is the boundary condition which stops recursion in its tracks
          or we could recurse for ever (or until Java runtime detects it and
          throws a StackOverflow exception!
        */
        if (n == 1) {
            long factorialOfN = 1L;
            ppr(1, factorialOfN);
            return factorialOfN;
        }

        // This where the magic really happens!
        long factorialOfN = n * recurse(n-1);
        ppr(n, factorialOfN);

        if (n == this.initialParameter) {
            printFinalResult(factorialOfN);
        }

        return factorialOfN;
    }

    // Helper methods to remove boilerplate
    private static void printPartialResult(long partialResult, int iteration) {
        System.out.println("Factorial("+iteration+"): "+ partialResult);
    }

    private static void ppr(int iteration, long partialResult) {
        printPartialResult(partialResult, iteration);
    }

    private static void printFinalResult(long factorialOfN) {
        // Format final result with thousand separators for emphasis!
        System.out.println("-------------------------------------------");
        System.out.println("=> " + NumberFormat.getInstance().format(factorialOfN));
        System.out.println("=> " + NumberFormat.getInstance().format(Long.MAX_VALUE) + " LONG.MAX_VALUE");
        System.out.println();
    }
}
