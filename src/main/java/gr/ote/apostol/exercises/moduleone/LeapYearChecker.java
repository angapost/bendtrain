package gr.ote.apostol.exercises.moduleone;

// Exercise 4
public class LeapYearChecker {
    public static void main(String[] args) {

        int[] testYears = {1700, 1800, 1900, 1600, 2000, 2004, 2056};

        // Test on exercise's sample years plus a few more!
        for (int testYear : testYears) {
            System.out.printf("Year %d is %sleap!\n", testYear,
                isLeapYear(testYear) ? "" : "not ");
        }
    }

    /**
     * Checks if a year is leap based on algorithm in exercise 4.
     *
     * @param year to test if it is a leap year
     * @return boolean result of the test
     */
    private static boolean isLeapYear(int year) {
        boolean isLeap = false;         // by default not leap!

        if (year % 4 == 0) {            // year is divisible by 4
            if (year % 100 != 0) {      // and NOT divisible by 100
                isLeap = true;              // then it is leap
            }
            else if (year % 400 == 0) { // if from a) not leap, and is divisible by 400
                isLeap = true;              // then it is leap
            }
        }
        return isLeap;
    }
}
