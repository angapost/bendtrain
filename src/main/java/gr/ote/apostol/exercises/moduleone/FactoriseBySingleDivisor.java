package gr.ote.apostol.exercises.moduleone;

import java.util.Scanner;

// Exercise 6
public class FactoriseBySingleDivisor {
    public static void main(String[] args) {
        int n = RunnerUtils.readInt("Give a natural number");
        int divisor = RunnerUtils.readInt("Give a divisor");

        if (divisor == 0) {
            System.out.println("Error: sorry cannot divide by zero!");
        } else {
            System.out.println(n + " = " + n / divisor + "*" + divisor + " + " + n % divisor);
        }
    }
}
