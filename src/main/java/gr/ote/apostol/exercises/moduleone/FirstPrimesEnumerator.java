package gr.ote.apostol.exercises.moduleone;

// Exercise 5
public class FirstPrimesEnumerator {
    public static void main(String[] args) {
        int n = 100;
        int numberOfPrimesUnderN = 0;

        boolean[] inPrimes = sieveOfEratosthenes(n);

        for (int i = 2; i <= n; i++) {
            //if (isPrime(i)) {
            if (inPrimes[i]) {
                numberOfPrimesUnderN++;
                System.out.printf("Number %d is prime!\n", i);
            }
        }

        System.out.printf("There are %d primes up to 100!", numberOfPrimesUnderN);
    }

    /**
     * Simplistic primality test that just tests Number-Theoretic prime number definition:
     *
     * <p><em>a prime number is a natural number greater than 1 that is not a product of two
     * smaller natural numbers</em>.
     *
     * <p>This is simplistic for enumerating all primes up to a number as it doesn't take
     * advantage of previous work. More efficient enumerating algorithms work by eliminating
     * non-primes as being multiples of already known primes and are generally called <em>sieves</em>.
     *
     * @param n natural number > 1
     * @return whether <code>n</code> is a prime number
     */
    public static boolean isPrime(int n) {
        /*
         Try to find a smaller number that exactly divides n.
         Note that it is not necessary to check all numbers up to n-1
         as it is sufficient to search up to sqrt(n)!
         As multiplication is symmetric candidate dividers larger than
         sqrt(n) would have already been discovered.
         */
        //for (int i = 2; i <= Math.sqrt(n); i++) {
        for (int i = 2; i < n; i++) {

            if (n % i == 0) {
                return false;
            }
        }
        return true;
    }

    /**
     * Computes all primes up to some number <code>n</code>. It works by using each
     * successively found prime number to cancel out all its multiples and thus sieves
     * remaining numbers as primes!
     *
     * @param n the size of the sieve
     * @return the sieve as a boolean array where each <code>true</code> item marks the
     *         number at that index as a prime, whereas a <code>false</code> denotes a
     *         <em>composite</em>.
     */
    public static boolean[] sieveOfEratosthenes(int n) {
        /*
            Elimination process can only be based on the lower half of the number range
            as numbers in the upper half will cancel out numbers out of range!
            Note creative use of bit-shifting to perform division by 2 and rounding-up!
         */
        int halfN = (n+1) >> 1;

        boolean[] sieve = new boolean[n+1];

        // Assume every integer from 2 onwards to be prime before eliminating
        for (int i = 2; i <= n; i++) sieve[i] = true;

        int prime = 2; // First prime number by definition

        while (prime < halfN) {

            // Eliminate as composites (non-primes) all multiples of current prime
            for (int composite = prime << 1; composite <= n; composite += prime) {
                sieve[composite] = false;
            }

            // Skip over eliminated composites to get next prime
            while ((++prime < halfN) && (!sieve[prime])) {
            }
        }

        return sieve;
    }
}
