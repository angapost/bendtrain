package gr.ote.apostol.exercises.moduleone;

// Exercise 2
public class Harmonic {
    public static void main(String[] args) {
        int n = 10;

        float harmonic = 0;

        for (int i = 1; i <= n; i++) {
            // Force decimal-number division on otherwise int operands
            harmonic += ((float) 1 / i);

            // Print successive values to get a sense that this series is actually
            // "divergent" in the Mathematical sense!
            System.out.println("Harmonic("+i+"): "+ harmonic);
        }
    }
}
