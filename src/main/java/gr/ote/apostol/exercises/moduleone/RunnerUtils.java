package gr.ote.apostol.exercises.moduleone;

import java.util.Scanner;

public class RunnerUtils {
    protected static float readFloat(String prompt) {
        System.out.println(prompt + ": ");
        Scanner userInput = new Scanner(System.in);
        return  userInput.nextFloat();
    }

    protected static int readInt(String prompt) {
        System.out.println(prompt + ": ");
        Scanner userInput = new Scanner(System.in);
        return  userInput.nextInt();
    }
}
