package gr.ote.apostol.exercises.moduleone;

// Exercise 3
public class DivisionCalculator {
    public static void main(String[] args) {
        System.out.println("Hint: decimal point must be provided in localized form!");
        float numerator = RunnerUtils.readFloat("Give a numerator");
        float denominator = RunnerUtils.readFloat("Give a denominator");

        if (denominator == 0.0f) {
            System.out.println("Error: sorry cannot divide by zero!");
        } else {
            float result = numerator / denominator;
            System.out.println("Divide " + numerator + " / " + denominator + " = " + result);
        }
    }
}
