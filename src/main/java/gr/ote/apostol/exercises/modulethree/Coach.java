package gr.ote.apostol.exercises.modulethree;

public interface Coach {
    void warmup();
    void train();
    void recover();
    boolean isExpertIn(TrainingCenter.Sports sport);
}
