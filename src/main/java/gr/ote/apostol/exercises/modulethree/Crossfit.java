package gr.ote.apostol.exercises.modulethree;

public class Crossfit implements Coach {
    @Override
    public void warmup() {
        System.out.println("Crossfit warmup: Jumping Jacks");
    }

    @Override
    public void train() {
        System.out.println("Crossfit training: Full Body Exercises Circuit");
    }

    @Override
    public void recover() {
        System.out.println("Crossfit recovery: Self-Myofascial Release (SMR)/Foam Rolling");
    }

    @Override
    public boolean isExpertIn(TrainingCenter.Sports sport) {
        return sport == TrainingCenter.Sports.CROSSFIT;
    }
}
