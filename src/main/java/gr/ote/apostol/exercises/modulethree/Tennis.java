package gr.ote.apostol.exercises.modulethree;

public class Tennis implements Coach {
    @Override
    public void warmup() {
        System.out.println("Tennis warmup: Half-Powered Serves");
    }

    @Override
    public void train() {
        System.out.println("Tennis training: Ball Machine");
    }

    @Override
    public void recover() {
        System.out.println("Tennis recovery: Ice Baths for Knees and Elbows");
    }

    @Override
    public boolean isExpertIn(TrainingCenter.Sports sport) {
        return sport == TrainingCenter.Sports.TENNIS;
    }
}
