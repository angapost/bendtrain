package gr.ote.apostol.exercises.modulethree;

public abstract class TrainingCenter {

    public static enum Sports {FOOTBALL, BASKETBALL, TENNIS, CROSSFIT}

    // This is a "factory-method" or "virtual-constructor" that pushes
    // responsibility for instantiation and thus coupling to Coach implementations
    // down to concrete subclasses!
    public abstract Coach createSportProgram(Sports sport);

    // However useful polymorphic behavior can still be completely expressed here!

    public void trainClientsFor(Sports sport) {
        /*
            Note how this client-oriented code is now decoupled from instantiation concerns.
            TrainingCenter clients will generally want to use a training program matching
            their sport without needing to worry who provides or how is this program provided.
         */
        Coach specialtyProgram = createSportProgram(sport);

        // Now apply specialty program
        if (specialtyProgram != null) {
            specialtyProgram.warmup();
            specialtyProgram.train();
            specialtyProgram.recover();
        } else {
            System.out.println("Sorry we currently don't provide specialty programs for " + sport);
        }
    }

    public static void main(String[] args) {
        // AngelosGym is renowned for its expertise in football and crossfit!
        TrainingCenter tc = new AngelosGym(Sports.FOOTBALL, Sports.CROSSFIT);

        tc.trainClientsFor(Sports.FOOTBALL);
    }
}

/**
 * A concrete subclass of TrainingCenter that encapsulates policy and specifics
 * of instantiating appropriate implementations of Coach interface. To make this
 * more interesting and closer to the business domain at hand more than one Coach
 * implementations are instantiated and pooled and then selected at runtime based
 * on requested "fitness criteria" (pun intended :-).
 */
class AngelosGym extends TrainingCenter {

    private Coach[] coaches = new Coach[Sports.values().length];

    /**
     * Creates and pools {@link Coach} implementations appropriate for the
     * specified <code>sports</code> where this concrete class specializes.
     *
     * @param sports
     */
    public AngelosGym(Sports... sports) {
        int i = 0;
        for (Sports s: sports) {
            Coach c = null;
            switch (s) { // Could really only provide cases for sports this class specializes to!
                case FOOTBALL:
                    c = new Football(); break;
                case BASKETBALL:
                    c = new Basketball(); break;
                case TENNIS:
                    c = new Tennis(); break;
                case CROSSFIT:
                    c = new Crossfit();
            };
            coaches[i++] = c;
        }
    }

    /**
     * Implementation of <em>factory method</em>.
     *
     * @param sport to instantiate a suitable Coach implementation for
     * @return a {@link Coach} implementation matching <code>sport</code>
     */
    @Override
    public Coach createSportProgram(Sports sport) {
        // This resolution process is also using another Design Pattern called
        // "Chain of Responsibility"
        for (int i = 0; i < coaches.length; i++) {
            Coach c = coaches[i];
            if (c != null && c.isExpertIn(sport)) {
                return c;
            }
        }
        return null;
    }
}
