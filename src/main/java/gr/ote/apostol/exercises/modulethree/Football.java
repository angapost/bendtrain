package gr.ote.apostol.exercises.modulethree;

public class Football implements Coach {
    @Override
    public void warmup() {
        System.out.println("Football warmup: Walking Lunges");
    }

    @Override
    public void train() {
        System.out.println("Football training: Dribble and Run");
    }

    @Override
    public void recover() {
        System.out.println("Football recovery: Epsom Salt Baths");
    }

    @Override
    public boolean isExpertIn(TrainingCenter.Sports sport) {
        return sport == TrainingCenter.Sports.FOOTBALL;
    }
}
