package gr.ote.apostol.exercises.modulethree;

public class Basketball implements Coach {
    @Override
    public void warmup() {
        System.out.println("Basketball warmup: Jump Shots");
    }

    @Override
    public void train() {
        System.out.println("Basketball training: Shoulder Presses");
    }

    @Override
    public void recover() {
        System.out.println("Basketball recovery: Contrast Showers");
    }

    @Override
    public boolean isExpertIn(TrainingCenter.Sports sport) {
        return sport == TrainingCenter.Sports.BASKETBALL;
    }
}
